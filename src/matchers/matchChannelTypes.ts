import { Matcher } from ".."
import { ChannelType } from "../types"
import { CHANNEL_TYPES } from "../constants"

/** Match messages from one of the given channel types. */
export const matchChannelTypes = (
  ...types: ChannelType[]
): Matcher => context => {
  const { message } = context
  const typeIds = types.map(type => CHANNEL_TYPES.findIndex(t => t === type))

  if (typeIds.includes(message.channel.type)) return context
}
