import { Matcher } from ".."

/** Match messages sent by this application. */
export const matchBot = (): Matcher => context => {
  if (context.message.author.bot) return context
}
