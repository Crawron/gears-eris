import { Matcher } from ".."
import { TextChannel } from "eris"

/** Matches messages from NSFW channels. */
export const matchNsfw = (): Matcher => context => {
  const { message } = context
  const channel = message.channel

  if (!(channel instanceof TextChannel)) return
  if (channel.nsfw) return context
}
