import { Matcher } from ".."

/** Match messages sent by the current bot. */
export const matchSelf = (): Matcher => context => {
  const { bot, message } = context
  const botId = bot.client.user.id

  if (message.author.id === botId) return context
}
