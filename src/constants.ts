export const CHANNEL_TYPES = [
  "TextChannel",
  "PrivateChannel",
  "VoiceChannel",
  "GroupChannel",
  "CategoryChannel",
  "NewsChannel",
  "StoreChannel",
] as const
