import * as Gears from "@enitoni/gears"
import { Message, Client, ClientOptions } from "eris"

export type AdapterOptions = {
  token: string
} & ClientOptions

export class Adapter extends Gears.ClientAdapter<
  Message,
  Client,
  AdapterOptions
> {
  register(options: AdapterOptions, hooks: Gears.AdapterHooks<Message>) {
    const { token, ...clientOptions } = options
    const client = new Client(token, clientOptions)

    client.on("messageCreate", hooks.message)
    client.on("ready", hooks.ready)
    client.on("disconnect", hooks.unready)
    client.on("error", hooks.error)

    return {
      client,
      methods: {
        start: async () => {
          client.connect()
        },
        getMessageContent: (message: Message) => message.content,
      },
    }
  }
}

export type Context<S extends object = {}> = Gears.Context<S, Message, Client>
export type Matcher<S extends object = {}> = Gears.Matcher<S, Message, Client>
export type Middleware<S extends object = {}> = Gears.Middleware<
  S,
  Message,
  Client
>

export class Command<D = unknown> extends Gears.Command<Message, Client, D> {}

export class CommandGroup<D = unknown> extends Gears.CommandGroup<
  Message,
  Client,
  D
> {}

export class CommandBuilder<D = unknown> extends Gears.CommandBuilder<
  Message,
  Client,
  D
> {}

export class CommandGroupBuilder<D = unknown> extends Gears.CommandGroupBuilder<
  Message,
  Client,
  D
> {}

export class Service extends Gears.Service<Message, Client> {}

export { matchBot } from "./matchers/matchBot"
export { matchChannelTypes } from "./matchers/matchChannelTypes"
export { matchNsfw } from "./matchers/matchNsfw"
export { matchSelf } from "./matchers/matchSelf"
