import { CHANNEL_TYPES } from "./constants"

export type ChannelType = typeof CHANNEL_TYPES[number]
